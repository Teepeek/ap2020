let myFont;
var noun;
var adjective;
var verb;
var adverb;

function preload() {
myFont = loadFont('data/minecraft.otf')
words = loadJSON("words.json");
}
  function setup() {
    createCanvas(800,600,WEBGL);
      fill(225); //this sets the basis for the text, its color, font and size.
      textFont(myFont);
      textSize(25);

noun = random(words.words[0].examples)
adjective = random(words.words[1].examples)
verb = random(words.words[2].examples)
adverb = random(words.words[3].examples)
}

function draw(){
  background(70, 35, 35);
rotateY(millis() / 3000);
fill(130, 70, 70,99);
box(150);
          nouns_can_be_substance();
          adjectives_can_be();
          verbs_can_be_actions();
          adverbs_can_express();
}

function nouns_can_be_substance() {
let words_ = push();
let can_have_ = translate(-60,0, 75);
let many_meanings_ = fill(225);
let are_let_ = text(noun,0,0);
let be_ = pop();
////////////////////////////////////////////
words_
can_have_
many_meanings_
if (frameCount %55 == 0) {
  noun = random(words.words[0].examples);
}
are_let_
be_
}

function adjectives_can_be() {
let be_something_ = push();
let to_adject_ = rotateX(0);
let on_us_ = rotateY(3.15);
let or_anything_ = translate(-70,25, 75);
let only_ = fill(225);
let they_ = text(adjective,0,0);
let are_ = pop();
///////////////////////////////////////////
be_something_
to_adject_
on_us_
or_anything_
only_
if (frameCount%40 == 0) {
  adjective = random(words.words[1].examples);
}
they_
are_
}

function verbs_can_be_actions() {
let are_showing_ability_ = push();
let to_develop_ = rotateX(0);
let omnipresential_ = rotateY(1.55);
let effect_ = translate(-50,50,75);
let in_essence_ = fill(225);
let are_to_ = text(verb,0,0);
let express_ = pop();
///////////////////////////////////////////
are_showing_ability_
to_develop_
omnipresential_
effect_
in_essence_
if (frameCount %30 == 0) {
  verb = random(words.words[2].examples);
}
are_to_
express_
}

function adverbs_can_express() {
let express_relation_ = push();
let of_time_ = rotateX(0);
let and_place_ = rotateY(-1.60);
let to_modify_ = translate(-60, -20, 75);
let utterances_ = fill(255);
let or_when_given_ = text(adverb,0,0);
let omnipotence_ = pop();
///////////////////////////////////////////
express_relation_
of_time_
and_place_
to_modify_
utterances_
if (frameCount %35 == 0) {
  adverb = random(words.words[3].examples);
}
or_when_given_
omnipotence_
}
