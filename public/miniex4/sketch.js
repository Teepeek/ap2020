  var x = 230;
  var button;
  var check;
let capture;

function setup() {
    createCanvas(1024,768);
  background(20);
    button = createButton('Start')
    button.position(512, 384)
    button.mousePressed(pressed); //This is connected to the defined function line 52.
    button.size(50,20);


    check = createCheckbox ('I accept terms and conditions', false);
    check.position(400, 150);
    check.size(210, 40);
    check.changed(checkEvent); //this is connected to the defined function line 67.
    check.style('color', '#E6E6FA');
    check.style('background-color', '#800000');
    check.hide();

  capture = createCapture(VIDEO);


  }


function draw() {
frameRate(12);

    if (mouseIsPressed) {
    console.log(mouseX, mouseY);
    }

//small rectangles that move (data)
    fill(255)
    rect(x, 114, 15, 15);
    rect(x, 350, 15, 15);
    rect(x, 550, 15, 15);
    x = x + 3
    if (x>715) { //715 = end of bars, 230 is the start
      x = 230;

}
if(capture.loadedmetadata) { //This is the camera function I tried to integrate.
  let c = capture.get(0, 0, 190, 190);
  image(c, 100, 35);
}

}
function pressed() { //this function is created to the button.
button.hide();
check.show();
  fill(255);
  rect (48, 50, 180, 175); //these rectangles are the white squares where the e.g. camera and audio wave should be.
  rect (48, 270, 180, 175);
  rect (48, 490, 180, 175);
  fill(0, 0, 0);
  stroke(40);
  strokeWeight(5);
  rect(718, 50, 280, 600);
}

/*I would have set the elements of the moving rectangles from function draw() into here,
but I didn't know how to make it work, as the code would only work in function draw for me. A solution would have been to define a new function and call it under function draw(), which I tried, but didn't make it work.*/
function checkEvent() {  //this is connected to the checkmark, so when one would press the checkmark, the camera etc. and data bars would appear.
check.hide();
if (this.checked (true)) {
  }




}
