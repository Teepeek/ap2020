![Screenshot](public/miniex4/miniex4.png)

[My MiniExercise](https://Teepeek.gitlab.io/ap2020/miniex4/)

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/miniex4)


## Questions to think about (README):

	
### Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.
			
_The initial title of the work would be "Lost in Data", addressing the feeling of confusing and information overload which is in no state of comprehension. The work attemps to communicate how we accept conditions and submit to giving over so much of ourselves that we can lose ourselves in the same process. In this sense, having so many capturing inputs around us which collect and release data, it can become hard for users to fall back into a true sense of self as they become engulfed in these data elements. It could seem as if the data which is being transfered back and forth feed off of people's identities and intentions on the web._ 
				
### Describe your program and what you have used and learnt.
		
_The program’s intention was to first have a button “start”, which led to a checkbox that asked the user if they would “accept terms and conditions” which refers to the common phenomenon where users would accept these terms without looking further into them, which would in this case lead to an immense leak or transfer of data. Here the small rectangles which slowly move across the canvas signify datatransfer from the user to the unknown which could be for statistics as well as cookies that record the user's browsing activity. 
In my program I have used some variables tied to DOM elements such as a button and a checkmark. To this I have added some small styling to make the DOM elements fit better with the background which was intentionally dark, trying to signify a gloomier or program-based setting. First by creating a button I make much use of functions such as button.show(); and button.hide(); which enabled me to hide and pull out new objects on the screen as the DOM elements were interacted with. I had issues with hiding some elements and first showing them elsewhere; here being the data cubes which were supposed to show up after the interaction with the checkbox (checkEvent). The issue with this was that the looping function of the rectangles that are moving, would only work under function draw(). What would make sense was to define a new function under the checkEvent() - even though not being possible – to create a link which I could call under function draw()._ 

### Articulate how your program and thinking address the theme 'Capture all'
	
_Including elements like a webcam appeal to how personal information is leakable and the vulnerability that is created through every step choice users could take which gives off tracks of data behind them. As data capture also includes statistical recommendations based off search history as well as advertising to gain profit, there is a lot that can be unknown to the average user as they are unknowingly becoming a method of exploitation and are a resource for businesses and networking websites such as Facebook._ 

### What are the cultural implications of data capture?

_Vulnerability and security on the web has become a raised issue as data also has a grasp in in areas of privacy. The fact that there are risks of not being safe on the web raises questions on the fact if there are any places at all that people would consider a safe space as life outside the web could give away less about their identity, interests and activities compared to the internet itself._



