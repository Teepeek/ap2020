
let earth;

function preload() {
earth = loadImage('data/earth.jpg');
}

function setup() {
createCanvas (800, 600, WEBGL);
print ("Welcome to instagram realness");
}

function draw() {
background(25, 20, 35);

//stars//
push();
noStroke();
fill(225)
circle(-335,-250,5);
circle(-250,-150,5);
circle(-240,-250,5);
circle(-350,-145,5);
circle(-300,-50,5);
circle(-200,-35,5);
circle(-100,-150,5);
circle(-50,140,5);
circle(-230,150,5);
circle(-135,70,5);
circle(-250,230,5);
circle(-340,150,5);
circle(-300,50,5);
circle(30,230,5);
circle(200,-30,5);
circle(250,-150,5);
circle(150,150,5);
circle(150,-200,5);
circle(50,-230,5);
circle(200,240,5);
circle(200,100,5);
circle(-200,200,5);
circle(50,-120,5);
circle(290,50,5);
circle(290,170,5);
pop();


//EARTH
push();
noStroke();
fill(90, 150, 175);
texture(earth);
rotateY(millis() / 13000); //makes earth rotate
sphere(54);
pop();

rotateY(millis() / 1600); //makes emojis rotate.

//EMOJI TOP - black & grey phone with blue screen, red heart.
//greyphone
fill(175, 175, 175);
rect(-170, -170, -110, 150);
fill(90, 200, 225)
rect(-180, -160, -90, 117);
fill(225, 225, 225)
circle(-225, -31.5, 20,)
//greyshadow
push();
noStroke();
fill(100, 100, 100);
rect(-170, -20, -4, -150);
rect(-170, -170, -110, 5);
pop();
// #1 heart
push();
noStroke();
  fill(255, 0, 0);
	ellipse(-240, -120, 30);
	ellipse(-210, -120, 30);
	fill('red');
	triangle(-255, -120, -195, -120, -225, -60);
pop();


//Bottom emoji
//faceshadow
push();
noStroke();
fill(225, 175, 160);
circle(113, 90, 104);
//face
noStroke();
fill(255, 204, 160);
ellipse(120, 90, 95, 103);
pop();

//eyeleft
fill(225,225,225);
ellipse(135, 80, 20, 25);

//eyeright
fill(225,225,225);
ellipse(105, 80, 20, 25);

//pupilleft
fill(0, 0, 0,)
ellipse(105, 85, 10, 15);

//pupilright
fill(0, 0, 0);
ellipse(135, 85, 10, 15);

//cheekbones
//leftbone
push();
noFill();
beginShape();
vertex(154, 80); //last argument changes end of vertex
bezierVertex(165, 100, 150, 100, 139.5, 111);
endShape();
//Rightbone
noFill();
beginShape();
vertex(83, 80); //last argument changes end of vertex
bezierVertex(75, 100, 90, 100, 100, 111);
endShape();
pop();

push();
//eyebrows
//lefteyebrow block
noStroke();
fill(67,38,1);
quad(95, 55, 115, 57, 114, 62.5, 95, 61);
//lefteyebrow point
fill(67,38,1);
triangle(74, 73, 95, 55, 95, 61);
//righteyebrow block
fill(67, 38, 1);
quad(126, 57.5, 144.5, 57, 144, 62.5, 126.5, 63);
//righteyebrow point
fill(67, 38,1);
triangle(144, 62.5, 144, 57, 161, 77);
pop();

/*Lipssssss
//toplips
lipleft */
push();
beginShape();
noStroke();
fill(225, 120, 125);
triangle(95, 117, 135, 117, 110, 105);
//lipright
fill(225, 120, 125);
triangle(103, 117, 145, 117, 130, 105);
endShape(CLOSE);

//bottom lip
beginShape();
noStroke();
fill(225, 100, 100)
quad(96, 117, 145, 117, 133, 130, 108, 130);
endShape();
pop();


/*This writes mousecoordinates in the console,
When you click on the screen (For the WEBGL 3D plane.) */
if (mouseIsPressed) {
console.log(mouseX-width/2, mouseY-height/2);

}

}
