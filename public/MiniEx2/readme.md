![Screenshot](public/MiniEx2/Screenshotofpiece.png)

[My MiniExercise](https://Teepeek.gitlab.io/ap2020/MiniEx2/)

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/MiniEx2)

## Questions to think about as README:

### Describe your program and what you have used and learnt.

_My program is built on a 3D plane. It includes a sphere (the earth), and two emojis that orbit the sphere. I have used a variable with the function preload() to insert and load the texture of the sphere, as well as a lot of "push();" and "pop();" to isolate the different pieces of code. First I spent a lot of time on the basis idea, then on considering which kind of emoji would illustrate that idea. I started by creating the sphere with a dark background with stars to add depth. I had chosen to make 2D emojis alluding to how we perceive them on digital artifacts, e.g. a smart phone - although the problem with WEBGL would make them flicker, as they moved around on the 3D plane. In the beginning it took a long time to create the elements such as emojis, placement of stars, etc. as it was quite hard and a bit confusing to code in the 3 axes. A lot of it was trial and error until I was able to recognize patterns in the code arguments when placing the positive and negative points on different geometric shapes. Towards the end of the process, I found a specific piece of code which provided feedback on the console when using the if mouseIsPressed() function to easily pinpoint different locations on the plane. This enabled me to code the placements of objects at a much faster rate._     


### How would you put your emoji into a wider cultural context that concerns representation,s identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).

_This piece reflects and mocks the culture of Instagram and social media in general. The idea behind the piece is that some people on e.g. Instagram can be obsessed with confirmation to gain attention such as "likes" and recognition. A many people go to extreme lengths e.g. using expensive plastic surgery and facial fillers to gain "the perfect look" as they're being inspired by other social media influencers; even though much of their inspirations is exaggerated or faked. By being able to make a living off of having a high social media presence through advertising and brand collaborations, for some people on this earth, these aspirations can be so important that it seems as if they are the only things that revolve around them. Therefore, it is the elements of social media culture that revolve around people on earth. In other words these things can seem so important that people forget about being themselves, but desire to become like the people they look up to who appear to have a high social standing and the perfect life. The emojis symbolize the perfection attension they so seek._ 






