![Screenshot](public/miniex6/miniex6.png)

[My MiniExercise](https://Teepeek.gitlab.io/ap2020/miniex6/)

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/miniex6)

_My game and the idea refer to what I wrote in the readme of my mini-exercise three: “[…] It's almost like a game of waiting […]”. I have taken that exact same black hole and have created a game where the player controls the black hole using the up and down arrow keys to eat as many planets as possible to keep surviving. Having a timer which is the lifespan of the black hole, if it reaches zero the game ends._

_In my separate .js file I have my class which is for the planets that move from right to left. The attributes I have included are the planets’ size, position and speed. I tried to include color in terms of giving the planets random colors individually every time they appear, but I figured that I had to create an array of objects having more than one object in my class. I knew how to insert them and tweak the multiple objects, but I didn’t know what to with the rest in the main sketch.js file._ 
 .
_I looked a lot toward Professor Soon’s code instructions from the class “Object Abstraction” to create an understanding and have a guideline on how to set up a class for example. A lot of my code has been inspired by Professor Soon’s, in terms of the resemblance of the game, where I have then removed and edited the code to my own liking and added extra elements such as the timer. To make the game end, I switched out the effect of the normal score with a timer. This made the score irrelevant for having a part to play in what determines if the player loses. The timer’s effect doesn’t really make the game into a “play to win” type of situation, where there is both winning and losing. Here there is only “surviving” by keeping the timer going as the player feeds the black hole planets and adds 1.5 seconds to the lifespan. In the way that I did this was simply by putting an if-statement under the function checkResult: “ if (timerValue== 0) […]”, then the game should stop looping and write the message “game over”._

_Object-oriented programming includes reusability of code. To not have to rewrite similar sets of code if one requires multiple elements as a result, the reusability is done in terms of writing a set of code once, which then can be reused in different programs._
_Inheritance is an example of reusability where classes and subclasses can be assigned with known properties and methods, where the programmer’s assumptions and expectations of the behaviour of the object after giving it specific properties here are met. When these are fulfilled for the programmer, inheritance allows for extending an object’s known behaviour with new additions. In abstraction sits the fact that there can be elements of what we interact with that are unknown to us, abstract materiality. We might know how to move the position of the black circle after reading the instruction, but we might not know how the object has gained these properties._

_When looking at a game like Minecraft, a big part of the game is centered on randomized generated objects. For example, one could guess that there is some kind of randomizer-code written into the game although there could be more to it. When a tree spawns in the game, it’s building blocks, height, thickness, kind, etc. all play a part in creating this object. The fact is though that the environment as an object itself decides what kind of tree should grow, in terms of deciding the kind of forest or terrain in general. This process is abstracted as the relations between objects in the game are more complex than what one would think - it generates a random value that picks out other pieces of coded objects to create a world which it defines as something we would e.g. call a birch forest._

**Reference list:**

Black hole from Mini-exercise 3: 
https://teepeek.gitlab.io/ap2020/miniex3/


Help and inspiration:
https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source%2F5-ObjectAbstraction

 https://editor.p5js.org/denaplesk2/sketches/ryIBFP_lG
