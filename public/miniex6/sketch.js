let blackholeSize = {
    w:110,
    h:103
};

let blackhole;
let holeposY;
let mini_height;
let r, g, b; //random colors for planets
let min_planet = 5;
let planet = [];
let score = 0, missed = 0;

var timerValue = 10;
var startButton;


function preload(){
  blackhole = loadImage("data/blackhole.png");
}

function setup() {
  createCanvas(1000, 600);
holeposY = height / 2;
mini_height =  height / 2;

  for (let i = 0; i <= min_planet; i++) {
    planet[i] = new Planet();
  }

  r = random(255);
  g = random(255);
  b = random(255);

  textAlign(CENTER);
  setInterval(timeIt, 1000);
}

function draw() {
  background(10, 10, 70, 85);
image(blackhole, 0, holeposY, blackholeSize.w, blackholeSize.h);
showPlanets();
checkPlanetsNum();
checkSwallow();
displayScore();
checkResult();

push();
fill(255, 255, 255, 15); //transparent box for the texts in left bottom corner.
rect(10, 445,700,150);
pop();

push();
textSize(20);
if (timerValue >= 10) {
    text("0:" + timerValue, width / 1.75, height / 1.25);
  }
  if (timerValue < 10) {
    text('0:0' + timerValue, width / 1.75, height / 1.25);
  }
  pop();
}

function timeIt() {
  if (timerValue > 0) {
    timerValue--;
  }
}

function displayScore() {
  textSize (17);
  text('Swallowed ' + score + " planet(s)", 110, height/1.2);
  text('⬆️arrow = UP  /  ⬇️arrow = DOWN', 155, height/1.3);
push();
  textSize(20);
  text('Swallow Planets to survive - or starve!', 190, height/1.05);
  textStyle(BOLD)
  text('Health:', width/2, height/1.25);
  pop();
push();
  fill(255);
  textSize(17);
  text('Reload page for new color planet or text (if too dark)', 221, height/1.14)
pop();
}

function checkResult() {
  if (timerValue == 0) {
    textSize(30);
    text("Black hole has starved. GAME OVER", width/2 , height/2 + 35);
    noLoop();
  }
}

function checkSwallow() {
  for (let i = 0; i<planet.length; i++) {
    let d = int(dist(blackholeSize.w/2, holeposY+blackholeSize.h/2, planet[i].pos.x, planet[i].pos.y));
      if (d < blackholeSize.w/2.5) { //close enough for black hole to swallow
      score++;
      timerValue = timerValue+1.5; //When the black hole eats, it gains 1.5 seconds in lifespan.
      planet.splice(i,1);
    } else if (planet[i].pos.x < 3) { //black hole missed the planet
      missed++;
      planet.splice(i,1);
    }
  }
}

  function checkPlanetsNum() {
    if (planet.length < min_planet) {
      planet.push(new Planet());
    }
  }

  function showPlanets() {
  for (let i = 0; i <planet.length; i++ ) { //controls each 'planet' to do their job.
    planet[i].move();
    planet[i].show();
    }
  }

function keyPressed() { //to make arrowkeys work to move the black hole
  if (keyCode === UP_ARROW) {
    holeposY-=30;
  } else if (keyCode === DOWN_ARROW) {
    holeposY+=30;
  }
  //reset if the blackhole moves out of range
  if (holeposY > mini_height) {
    holeposY = mini_height;
  } else if (holeposY < 0 - blackholeSize.w/2) {
    holeposY = 0;
  }
}
