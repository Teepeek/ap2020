class Planet {
  constructor() {
    this.speed = random(2,4);
    this.size = random(15,55);
    this.pos = new createVector(width+5, random(0,350));
    //this.color = color(random());
  }

  move() {
    this.pos.x -= this.speed; // for each iteration, the Planets will decrease on the x-axis, and move from right to left
  }

  show() {
    fill(r,g,b);
    ellipse(this.pos.x, this.pos.y,this.size, this.size,);
  }

}
