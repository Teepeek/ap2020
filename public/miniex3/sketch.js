var sp = 0.0; //speed
var sz = 0.0; //size
let x = 2; //translate 2

function setup() {
  createCanvas(800, 600);
frameRate (30);
ellipseMode(CENTER);
}

function draw() {
  background(10, 10, 70, 85);
//transparency of background gives trail and fading effect on throbber.

//Black hole / Portal thing
  //transparent shadows
  push();
  translate (width/ x, height / x); //moves things to center.
    noStroke();
    fill (200, 200, 200, 5);
    ellipse(0, 0, 90, 75)
    ellipse(0, 0, 70, 90)

//other shadows
    fill (200, 200, 200, 5);
    ellipse(0, 0, 90, 55)
    ellipse(0, 0, 95, 85)

//another shadow
    fill(200, 200, 200, 5)
    ellipse(0,0, 100, 100)
  pop();

//hole
  push();
  translate (width / x, height / x);
    noStroke();
    fill(40, 45, 95) //this is the small blue barrier around hole
    ellipse(0, 0, 65, 65)
    fill(0);
    ellipse(0, 0, 50, 50) //hole itself :)
  pop();

//Moving planets / orbs thingies
  sp = sp + 0.0147; //change speed
  sz = cos(sp)*2.5; // change size

push();
translate(width / x, height / x);
scale(sz); //this makes the elipse do that scaling thing (transforming motion).
  strokeWeight(0.5);
  stroke(148, 103, 51);
  fill(216, 150, 74, 50)
  ellipse(40, 30, 50); //bigball

translate(width/140, 0);
scale(sz);
  strokeWeight(0.5);
  stroke(166, 8, 27);
  fill(199, 0, 57, 50);
  ellipse(-23, 10, 12, 12); //left low ball

  strokeWeight(0.08);
  stroke(100, 198, 213);
  fill(70, 158, 196, 50)
  ellipse(10, -20, 5, 5); //right small top ball
pop();
}
