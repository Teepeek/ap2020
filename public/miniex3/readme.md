![Screenshot](public/miniex3/screenshotofminiex3.png)

[My MiniExercise](https://Teepeek.gitlab.io/ap2020/miniex3/)

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/miniex3)


## Questions to think about (README):
_Describe your throbber design, both conceptually and technically._ 
	
### What is your sketch? What do you want to explore and/or express?
			
_My idea was a black hole with planet-like elipses warping in and out endlessly. I want to express how expectations can be failed and things "can go in circles" with throbbers, e.g. with a loading screen you can expect it will finish at some point but unexpectedly, it adds more time. With this black hole, it is meant that the elipses would continuously be moving in and out of the black hole and continues this animation until the loading is done It's almost like a game of waiting where one would have to see if it would spit out the planets again._
_The throbber in this piece appears to act as a form of diversion. Time is present as the elipses slow in speed when becoming visible to the user. In the attempt for the diversion to feed the user a form of "entertainment" by throwing elipses in at the user's field of view, When the throbber throws elipses at the user's field of view, it becomes an attempt at creating a diversion or distraction to feed the user a form of "entertainment", as processes are taking place. The user's attention in this short space of time is meant to simultaneously be severed and gained as the throbber shifts in movement._ 
				
### What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?
		
_With the scale function I expand and contract objects over a specified amount of time - here transforming the speed and size while using the translate functions to choose where the contracting objects should expand to. I have used the scale function with a slower speed, which makes it appear as the planets slow down as they rise, while quickly falling down to the hole again. In computation time can be constructed in terms of being aware of time itself. Having a control in specific elements of code which enhance the feeling of time makes the throbber in this piece able to have a continuity of a feeling of distruption and unification of the user's attention and how they experience time._



### Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?
	
_There are a lot of differences in what throbbers communicate when comparing those of webbrowsers to video game throbbers. In some video games throbbers are purposely built with an aesthetic to not only signify a passing of time but also that these throbbers can communicate how we would percieve waiting-time differently._ 