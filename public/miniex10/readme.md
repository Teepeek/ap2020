## Group idea 1
![Screenshot](public/miniex10/idea1.jpg)

## Group idea 2
![Screenshot](public/miniex10/idea2.jpg)

## My individual flowchart - Based on mini exercise 6
![Screenshot](public/miniex10/myflowchart.jpg)

## Questions to think about for the README
 
### What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

When creating flowcharts, one must have the “target group” or aim in mind for whom they want to facilitate the chart. One must not only be able to use a flowchart as a boundary object to ensure that all members of a project with widely different competences understand what one is communicating, but also for oneself to understand the work at hand. Oversimplifying or overcomplicating flowcharts can make the work for oneself become challenging. When having to facilitate idea to members of a project, it is best to imagine that other members might not know what e.g. the source code of the program is. To get into it as easily as possible a flowchart is to be included to allow for easy passage into understanding the task at hand. (Ensmenger, 2016: 33-37)

Within algorithmic procedures, there are many kinds of solutions that can be used and with an algorithm, it becomes only a proposal of many possible solutions. It is therefore important to carefully plan the steps and sequences that take place in order to have a most efficient process as possible. (Bucher, 2018: 23) When having solutions as a focus, algorithms don’t have a power in themselves as they only show the possible outcomes; they are the anchor to translate, coordinate and structure procedures which can otherwise be impossible for a person on the outside to outside to understand. (Bucher, 2018: 37)

### What are the technical challenges for the two ideas and how are you going to address them?

Technical challenges we wondered we might run into revolved around our own programming abilities and how we might tackle problems without having to change too much of the programs, if they at length would begin to become dissimilar to the flowchart. This could as a consequence render the flowchart useless as it wouldn’t be a correct representation of the program.

### What is the value of the individual and the group flowchart that you have produced?

For the group flowcharts, they are based more on the concept and overall structure of a future program or idea, which doesn’t yet include too many and too complex technical elements. They are a lot more straightforward as they follow a straighter line of procedures that take place after one another.

In my own flowchart I had a hard time figuring out how I should include and exclude elements without making the flowchart too complex or too simple taking into account that it is to be shown to others who wouldn’t necessarily know my program and the structure of it. I am not sure if my outcome is an acceptable way of making a flowchart, but I decided to make three branches which describe the three main processes that happen in the program which I find most important. To make it more structured and easier on the eye, I color-coded each section so the reader would be able to have an easier time at figuring out what belongs to what. At the top of the flowchart I have made some boxes more pronounced than others making them the overall divider for each section. In this flowchart I have both attempted to look at the overall concept and the source code in order to make a precise but somewhat understandable flowchart. 

**Reference list:**

Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." Information & Culture: A Journal of History, vol. 51 no. 3, 2016, pp. 321-351

Bucher, Taina. If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms")
