![Screenshot](public/miniex5/miniex5.png)

[My MiniExercise](https://Teepeek.gitlab.io/ap2020/miniex5/)

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/miniex5)

_A side note: I had some issues with the button function to make a second triangle show up when pressing "hurt". I have no idea what the issue is and why it is not showing up._


## Questions for readme.md
### What is the concept of your work?
_The concept for this piece beforehand was: " there still can be issues regarding those who are living among minorities are accepting and have a progressive state of mind, might not be able to see - or often forget, how minorities, e.g. members of the LGBTQ+ community and women still are living with an underlying threat of discrimination in progressive societies; even as this theat isn't as clear, compared to how discrimination took place earlier in history. This is also meant with regard to the societal norms in which many still have a mindset where gender and identity that wasn't within the norm before, accidentally becomes characterized which isn't necessarily in a bad way, but still puts people into "social boxes"._

_The evolved piece now talks about how people have an option to hurt or passively discriminate others and the level of discrimination and pain (sliders) that is caused varies greatly depending on the option._

### What is the departure point?
_The program is supposed to be based on when the user slides the sliders which control the spike of hate, pain etc. towards the button discriminate, it enables the button add more red splinters. The red and blue shades signify the people who aren't a minority or that they live in a progressive society. The yellow shade represents the minorities, and how there is a side (on the box) that isn't easily recognizable to the red and blue shades compared to the yellow one, where the minorities face an underlying 'threat'._
### What do you want to express? What have you changed and why?
_I want to express my own thoughts and opinions on the subject through an "abstract" piece which is now partly controllable by the user. I have added DOM-Elements to the piece for the user to have a possible interaction which could show their possible influence on the matter. The elements added are two sliders and a button._
### Reflect upon what aesthetic programming might be (based on readings like class01 on Why Program? And Coding Literacy)
### What does it mean by programming as a practice or even as a method for design? Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?
_Talking from Nick Montfort's "Why Program?", aesthetic programming is especially relevant for those studying digital design as it's not only general design but with computation as a big focus. Programming or Aesthetic programming here can give insight into cultural systems, aiding the designer's development of their collaborativity and perspective. My take on the relation between programming and digital culture is that communication is important for all parties in a team when working together._

