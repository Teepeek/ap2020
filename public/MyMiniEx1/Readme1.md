_Here is my piece for the first mini-exercise. What I thought of while creating this piece was how there still can be issues regarding those who are living among minorities are accepting and have a progressive state of mind, might not be able to see - or often forget,  how minorities, e.g. members of the LGBTQ+ community and women still are living with an underlying threat of discrimination in progressive societies; even as this theat isn't as clear, compared to how discrimination took place earlier in history. This is also meant with regard to the societal norms in which many still have a mindset where gender and identity that wasn't within the norm before, accidentally becomes characterized which isn't necessarily in a bad way, but still puts people into "social boxes"._  

[MyMiniExercise](https://Teepeek.gitlab.io/ap2020/MyMiniEx1/)

[Screenshot of Piece](https://imgur.com/a/d3DVq9J)

## Questions to think about in your README:

### How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?

####  In the beginning I found it difficult and even frustratiing to figure out how some codes interfered with others, when inserting them. I figured out that there was a certain structure needed, to be able to make everything work together. It became satisfying to see these parts finally complete and create a desired image in the atom-live-server.

### How is the coding process different from, or similar to, reading and writing text?

#### Differently from writing text, I needed to go back and forth from different paragraphs, inserting codes and taking others out in e.g. function setup and function draw; even though I didn't find that some lines of code had anything to do with the paragraphs they were put in. Although, it seemed, that the top part of the "coding page" had the most important parts that set the boundaries in which the inserted objects of code would play their parts in creating an image. 	


### What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?

#### That the small things in Coding can mean alot in terms of provoking and raising awareness to societal issues, etc. E.g. A simple piece of programmed artwork can say a lot about the artist's or programmer's values and ideas. The assigned reading, e.g. "Why Program?" spoke a lot to me about how programming or coding is not only about interting a bunch of numbers in a specific programming language to create an instructed piece; but has the ability to stimulate thinking, and can help explain why it is important to learn for artists and designers who work in a team.