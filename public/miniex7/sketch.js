var r;
var g;
var b;

var x;
var y;

var branches = [];
var min_branches = 4;

function setup(){
 createCanvas(800, 600);
 frameRate(144);

 for (let i = 0; i < min_branches; i++){
   branches[i] = new Branch();
 }

 r = random(255);
 g = random(r);
 b = random(b);

 /*the background is a random value between 10 and 50
 for each rgb value everytime the page is refreshed*/
 background(random(10, 50), random(10,50), random(10,50));
}

function draw(){
  for (let i = 0; i < branches.length; i++){
    branches[i].show();
    branches[i].move();
  }


}

class Branch{
  constructor(){
    this.x = width/2;
    this.y = height/2;
    this.size = 4;
  }

  move(){
    this.x = this.x + random(-7,7);
    this.y = this.y + random(-7,7);

    /* if the x value is less than zero it will continue its path at width.
    These next lines of code gives the ellipses the ability to teleport to the
    opposite side of the canvas when disappearing.
    It gives the illusion of continuity.*/
    if (this.x < 0){
      this.x = width;
    }

    /* if the x value is greater than width it will continue its path at 0 */
    if (this.x > width){
      this.x = 0;
    }

    if (this.y < 0){
      this.y = height;
    }

    if (this.y > height){
      this.y = 0;
    }

  }

  show(){
    fill(0,g,b);
    noStroke();
    r += random(-10, 50);
    r = constrain(255, r, g); //constrain limits or restricts the rgb values which here affect the hues of the blueness on the ellipses.

    g += random(-100, 80);
    g = constrain(100, g, b);

    b += random(-15, 255);
    b = constrain(r, 0, g);


    ellipse(this.x, this.y,random(this.size),random(this.size));
}

}
