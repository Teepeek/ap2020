![Screenshot](public/miniex7/miniex7.png)

[My program](https://Teepeek.gitlab.io/ap2020/miniex7/)
_For some reason my link suddenly stopped working so I would like to refer you to Pernille's Gitlab to run the program from hers._

[My Repository](https://gitlab.com/Teepeek/ap2020/-/blob/master/public/miniex7)

## Auto Generative program made in collaboration with Pernille Johansen
[Link to Pernille's Gitlab](https://gitlab.com/pernwn/ap2020)


## Questions to think about as README:

### What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviours? 


*  Every time the page is refreshed the elements should create a new pattern. 
*  Every time the elements move randomly, they should change size;
*  Every time the elements move outside the canvas, a new one shall spawn at the opposite end of the canvas. 
*  Every time the program resets, the background should change its color.

The program is set to have a minimum of 4 “branches” as we call them. It’s more an array of elements moving together, for example like a flock of birds. The branches move randomly across the canvas and in different directions. We wanted it to be hypnotising and beautiful in a way and we wanted it to move “gracefully”. That’s why we added the rule of “every time the elements move outside the canvas, a new one shall spawn at the opposite end of the canvas”, thus making it seem like it moves continuously. Over time the canvas will be filled with different sized ellipses filling every nook and cranny, though this might take some time and requires patience from the observer. 

When creating a generative problem and restricting that same program with a specific set of rules, the artist makes decisions based on an intentional aesthetic which then becomes encoded into the system. The system will use these sets of rules and start an autonomous process of running the artist’s carefully placed decisions. Referring to Philip Galanter’s definition of generative art, the decisions that the system makes contribute to the potentially completed work of art, although of which doesn’t necessarily result in a completed work of art (Watz, 2007, 11:50). When attempting to define what ‘completed’ means for our program, we would put emphasis on the satisfaction of the user. 

### What's the role of rules and processes in your work?
The ellipses which move in our program are given certain instructions to behave like they do. The instructions is to move “randomly” across the canvas in an up/down pattern. It is not a straight and fixed line which moves from one direction to another. The instructions make them move automatically without further guidance, so it seems that it behaves freely. The user receives an unexpected result of a pattern every time they reload the program, as both the background might change its color and the ellipses shift their direction of movement and create a new pattern.

We might argue that our program is reminiscent of Dada as the images “derive from mechanical processes not under the artist’s control” (Montford et al. 2012:124). Because the pattern is different every time you open the page anew or refresh, the images created are not under our control, but the given instructions (mechanical processes) determine what the pattern might be. 

### Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?

Our program can be described as generative art, as it focuses on the result or the outcome which come to play independently from us who have created the program. “Generative art describes processes defined by rules, processes which are automated to a certain degree by the use of a machine” (Arns, 2005:2) Our program generates a pattern, an image which is the result of unpredictable processes which take place as the program continuously runs and doesn’t stop. In the same way that there could be a result -  as our program never stops running - it could be up to the user to decide based on their satisfaction to make it up to themselves to be interested in the (possible for them but impossible for the program) result. The user has the possibility to take a screenshot or restart the program when they feel satisfied with the pattern when it has reached a certain state. 

Can it truly be random when it comes to computing? When given a set of instructions and rules we design the program to do certain things, how then, will autonomy be played out? Is it truly autonomous when we, as programmers, set boundaries for what it can or should do? We have made some reflections toward pseudorandomness which is “The production of random-like values that may appear at first to be some sad, failed attempt at randomness, but which is useful even desirable in many cases” (Montford et al. 2012:120). In the chapter it is used mainly when describing a “pseudorandom number generator”. Though we took it into consideration as the program creates “random-like” values. There is only some degree of autonomy as we set the instructions and rules for what the elements should do, but as they move along the x- and y-axis they do this by themselves. 


**References**
Montfort, N, et al. "Randomness". 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146 

Arns, Inke. “Code as Performative Speech Act.” Artnodes, vol. 0, no. 4, May 2005. Crossref, doi:10.7238/a.v0i4.727.

Marius Watz, "Beautiful Rules: Generative Models of Creativity", in The Olhares de Outono (2007), https://vimeo.com/26594644.
